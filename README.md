# back_webbylab


App uses sqlite DB and Koa.js
--------------------------------
At each start of app, app checks if db and tables exist, and if not - then db and tables are created.
All information about films is stored in table films.
Film contains next fields: 
    id - key, autoincremented field, 
    name - TEXT, 
    release_year - DATE, 
    format - TEXT, 
    stars - TEXT, 
    describe - TEXT)
--------------------------------
*** FOR FUTURE - table format for stored format of films and table stars for stored star name -> in table films stored array with id stars
--------------------------------
All dependencies are described in package.json and are installed by "npm i". 
--------------------------------
Start app:
At command line to start app may be used local port (PORT) and base(BASE) name to store ("node index.js PORT:8888 BASE:films.db"), if they are not present app uses default value for port and base.

Command for start app:
npm run dev - run for develop app
npm run start - run on server environement, start with forever, forever must install global with root priveleges
npm run test - run for testing

All params from requests to be validated by XSS injection - process is not stopped, but returns filtered params
--------------------------------
URL for API:

/films-import   method POST send file from form-data
use for send text file and parse information from file, then store in DB

/search/   method POST
use for search by name or by stars, use json in raw-body, params must be arrays and use next format:
 {
    "name": [
        "Rock"
        ],
    "stars": [
            "shon connery" , "bla bla"
        ]
}

/films   method GET
use for get all films

/films   method POST
use for create film, use json in raw-body next format:
[{
    "title": "new name",
    "release_year": 1970,
    "format": "dvds",
    "stars": "NEW star"
}]

/films/:id  method GET
use for get information by film by id

/films/:id  method DELETE
use for delete film by id

/films/:id  method PUT
use for update film by id, body must contain value: name, release_year, format, stars
--------------------------------