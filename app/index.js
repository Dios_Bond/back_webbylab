const config = require('./config/default');
const argv_config = require('./controllers/cliArgv');
const Koa = require('koa');
const routers = require('./routes/apiRoutes');
//const homeRout = require('./routes/homeRoutes');
const cors = require('@koa/cors');

const portNum = argv_config.PORT || config.server.PORT; // use default port is not parameter 

require('./controllers/startDBfunc'); // check and create db and table if not exists

const app = new Koa();


app.use(cors());
app.use(routers.routes);
app.use(routers.allowedMethods);

module.exports = app.listen(portNum, function () {
    let d = new Date();
    console.log('server app start at port ', portNum, " " + d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes());
});
