const config = require('../config/default');
const sqlite3 = require('sqlite3').verbose();

const dbName = require('../controllers/cliArgv').BASE || config.db.name;

const tabFormat = config.tables.tableFormat;
const tabStars = config.tables.tableStars;
const tabFilms = config.tables.tableFilms;


  //db.close();

    let db = new sqlite3.Database(dbName);

    const controller = {
            
            getFormat: function (){
                
                let dataPromis = new Promise((res,rej) => {
                    db.all(`SELECT * FROM ${tabFormat};`, (error, rows) => {
                        if (error){
                            console.log("Error ", error)
                        }
                        else {
                            res(rows);
                        }
                    })
                });
        
                return dataPromis;
            },

        //* get all Stars
        getAllStars: function (){
        
            dataPromis = new Promise((res,rej) => {
                db.all(`SELECT * FROM ${tabStars};`, (error, rows) => {
                    if (error){
                        console.log("Error ", error)
                    }
                    else {
                        res(rows)
                    }
                }
                )     
            })
        
            return dataPromis
        },

        //* get all films sorted by name
        getAllFilms: function (){

            let dataPromis = new Promise ((res,rej) => {
                db.all(`SELECT * FROM ${tabFilms} ORDER BY name ASC;`, (error, rows) => {
                    if (error){
                        console.log("Error ", error)
                    }
                    else {
                        res(rows)
                    }
                })
            });

            return dataPromis
        },


        //* get film by id
        getFilmById: function (id){

            let dataPromis = new Promise ((res,rej) => {
                db.all(`SELECT * FROM ${tabFilms} WHERE id=${id};`, (error, rows) => {
                    if (error){
                        console.log("Error ", error)
                    }
                    else {
                        console.log(rows)
                        res(rows)
                    }
                })
            });

            return dataPromis
        },

        //* search id star by name
        searchIdStar: function (star){
            let dataPromis = new Promise ((res,rej) => {
                db.all(`SELECT id ${tabStars} WHERE name=${star};`, (error, rows) => {
                    if (error){
                        console.log("Error ", error)
                    }
                    else {
                        res(rows)
                    }
                })
            })
        },

        //* create Star
        createStars: function (arr) {
            let stmt = db.prepare(`INSERT INTO ${tabStars}(name) VALUES (?)`);

            arr.forEach(elem => {
                stmt.run(elem);
                })
        },

        //* create film
        createFilms: function (data){
            let stmt = db.prepare(`INSERT INTO ${tabFilms}(name, release_year, format, stars, describe) VALUES (?,?,?,?,?)`);

            data.forEach(elem => {
                stmt.run(elem.title, elem.release_year, elem.format, elem.stars, elem.describe);
            })
        },

        //* delete film
        deleteFilmById: function (id){
            db.run(`DELETE FROM ${tabFilms} WHERE id=?`, id, (err => {
                if (err) {
                    console.error(err.message);
                }
            })
            )
        },

        //* update film
        updateFilmById: function (id, data){
            db.run(`UPDATE ${tabFilms} SET name=?, release_year=?, format=?, stars=? WHERE id=${id}`, data , (err => {
                if (err) {
                    console.error(err.message);
                }
            })
            )
        }
    };
    

    module.exports = controller;

    
