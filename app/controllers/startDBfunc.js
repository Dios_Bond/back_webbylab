const config = require('../config/default');
const sqlite3 = require('sqlite3').verbose();
const dbName = require('../controllers/cliArgv').BASE || config.db.name;


const dataFileFormat = require('../config/default_format.json');
const tabFormat = config.tables.tableFormat;
const tabStars = config.tables.tableStars;
const tabFilms = config.tables.tableFilms;



function createDb() {
    console.log("create Db", dbName);
    db = new sqlite3.Database(dbName);
    //db = new sqlite3.Database(dbName, createTable);
};

function createTableFormat() {
    console.log("createTable Format");
    db.run(`CREATE TABLE IF NOT EXISTS ${tabFormat} (id INTEGER PRIMARY KEY, format_name TEXT)`, insertFormat);
};

function createTableStars() {
    db.run(`CREATE TABLE IF NOT EXISTS ${tabStars} (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)`);
};

function createTableFilms() {
    db.run(`CREATE TABLE IF NOT EXISTS ${tabFilms} (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, release_year DATE, format TEXT, stars TEXT, describe TEXT)`);
};

function insertFormat() {
    console.log("start insert Format");
    let stmt = db.prepare(`INSERT INTO ${tabFormat} VALUES (?,?)`);

    dataFileFormat.forEach(elem => {
        stmt.run(elem.id, elem.format_name);
        })
    console.log("create table from JSON default_FORMAT.json")
    //stmt.finalize(readAllRows);
};


let db = new sqlite3.Database(dbName, (err) => {
    if (err) {

        console.log(err);
        if (err.errno == 14) {
            console.log("True")
            createDb();
            
        }      //return console.error(err.message);
      
    }

    console.log('Connected to SQlite database.');

  });

  //** create table if not exists */
  function createTable(table){
    db.get(`SELECT name FROM sqlite_master WHERE type='table' AND name='${table}'`, (error, searchTable) => {
        //console.log("table exists ", searchTable);
        if (searchTable == undefined) {
            console.log("no table", table);
            switch (table) {
                case tabFormat :
                    createTableFormat();
                    break;
                case tabStars :
                    createTableStars();
                    break;
                case tabFilms :
                    createTableFilms();
                    break;

            }
      }
    })
  };
  
  createTable(tabFormat);
  createTable(tabStars);
  createTable(tabFilms);
