//** This controller search films by param */

const config = require('../config/default');
const sqlite3 = require('sqlite3').verbose();

const dbName = require('../controllers/cliArgv').BASE || config.db.name;

const tabFilms = config.tables.tableFilms;

let db = new sqlite3.Database(dbName);

function ReturnQuery(opt){

    //**Simple query search */
    if (Object.keys(opt).length == 1){
        for (let prop in opt) {
            return `SELECT * FROM ${tabFilms} WHERE ${prop} IN (${opt[prop].map(el => { return `'${el}'`}).join()});`;
        }
    }

    //**Multi query search */
    else if (Object.keys(opt).length > 1){
        
        var str = `SELECT * FROM ${tabFilms} WHERE`;

        Object.keys(opt).map( (el, index) =>{

            if (index > 0) {
                str = str + ' AND';
            };

            str = str + ` ${el} IN (${opt[el].map(el => { return `'${el}'`}).join()})`;

            if (index == Object.keys(opt).length - 1) {
                str = str + ';';
            };

        });
        return str;
    }
}

function SearchFilms (opt){
    if (opt) {

        dataPromis = new Promise((res,rej) => {
            db.all(
                ReturnQuery(opt),
                //`SELECT * FROM ${tabFilms} WHERE name IN ('${opt.title.join()}');`,
                async (error, rows) => {
                if (error){
                    console.log("Error ", error)
                }
                else {
                    res(rows);
                }
            }
            )     
        })
        
        return dataPromis;
    } 
    

};

module.exports = SearchFilms;