/* function for control data on contains XSS */ 
function checkXSS(inp){ 	
    return inp.replace(/<\/?[^>]+>/gi, '');	
};

module.exports = checkXSS;