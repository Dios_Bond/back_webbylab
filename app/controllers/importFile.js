const fs = require('fs');
const readline = require('readline');
const CRUD = require('../controllers/crud');
const ModelFilm = require('../model/film');

module.exports = async function (tmpPathFile){
    //console.log(tmpPathFile)
    fs.readFile(tmpPathFile, 'utf8', function(err, data){
        if (err) throw err;
        //console.log(data.split('/\s*\n\s*/', 4));
        //console.log(JSON.stringify('[{' + data.split('\n\n\', 4)) + ']'));
        //spl = data.split('\n\n');

        result_arr = data.split('\n\n')
        .map(el => {
            return el.split('\n')
        })
        .map (el => {
            let Movie = new ModelFilm()
            el.map ( child_el => {
                
                let [name, val] = child_el.split(': ');
                Movie.putData(name, val)
                //console.log(Movie)
            });
            return Movie;
        });

        console.log('Finish ARR', result_arr);
        if (result_arr.length > 0){
            CRUD.createFilms(result_arr);
        }

        })
    }