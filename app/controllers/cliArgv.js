//**Convert command line named parameters argv to used form */

argum_config = process.argv.slice(2)
.map(arg => arg.split(':'))
.reduce ((args, [val, key]) => {
    args[val] = key;
    return args},
{});

module.exports = argum_config;