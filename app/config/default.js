module.exports = {
    server: {
        port: 8080
    },
    tables: {
        tableFormat: 'format',
        tableStars: 'stars',
        tableFilms: 'films',
    },
    db: {
        name: 'films.db'
    }
}; 
