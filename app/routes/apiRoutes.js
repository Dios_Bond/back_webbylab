const Koa = require('koa');
const bodyParser = require('koa-bodyparser'); 
const koaBody = require('koa-body');
const convert = require('koa-convert');
const Router = require('koa-router');
const importFile = require('../controllers/importFile');
const searchFilms = require('../controllers/searchFils');

//const json = require('koa-json');

const CRUD = require('../controllers/crud');
const Validate = require('../controllers/xssFilter');

const app = new Koa();
let router = new Router(app);
//app.use(json());
//app.use(bodyParser());

let KoaBody = convert(koaBody());
//* Router
    router
    //* reserved for web page
        .get('/', bodyParser(), async (ctx, next) => {
            console.log('home Route Requested with');
            //ctx.status = 200;
            //* add allow CORS
            //ctx.set('Access-Control-Allow-Origin', "*");
        })

        //** get all films */
        .get('/films', bodyParser(), async (ctx, next) => {
            //console.log('detail Route Requested with', ctx.req.url);
            ctx.set('Access-Control-Allow-Origin', "*");    //* add allow CORS
            let data = await CRUD.getAllFilms();
            ctx.response.body = data;
            ctx.status = 200;
        })

        .get('/films/:id', bodyParser(), async (ctx, next) => {
            //console.log('detail Route Requested with', ctx.req.url, ctx.params.id);
            ctx.set('Access-Control-Allow-Origin', "*");             //* add allow CORS
            let data = await CRUD.getFilmById(Validate(ctx.params.id));
            ctx.response.body = data;
            ctx.status = 200;
        })

        .put('/films/:id', bodyParser({multipart: true, urlencoded: true}), async (ctx, next) => {
            console.log('PUT Route Requested with', ctx.params.id);
            let id = Validate(ctx.params.id);
            let dataReq = Validate(ctx.request.body);
            let data = [dataReq.name, dataReq.release_year, dataReq.format, dataReq.stars];
            CRUD.updateFilmById(id, data);
            ctx.status = 200;
        })

        .post('/films', bodyParser({ 
            extendTypes: {
            json: ['application/x-javascript']}
            }), async (ctx, next) => {
                console.log('POST Route for Created Requested with');
                ctx.accepts('application/json', 'text');
                ctx.set('Access-Control-Allow-Origin', "*");
                let data = Validate(ctx.request.rawBody);
                CRUD.createFilms(JSON.parse(data))
                ctx.status = 200;
        })

        .delete('/films/:id', bodyParser(), async (ctx, next) => {
            let id = Validate(ctx.params.id);
            if (typeof parseInt(id) == 'number') {
                CRUD.deleteFilmById(id);
                ctx.status = 200;
            }
            else {
                ctx.status = 304;
            }

        })

        .post('/films-import', koaBody({
            //formidable:{uploadDir: './uploads'}, //for future, if need save file not in temp directory
            multipart: true, urlencoded: true }), async (ctx, next) => {
            ctx.accepts('txt');
            const file = ctx.request.files;
               
            if (file.file.type = 'text/plain'){
                ctx.status = 200;
                importFile(file.file.path);
            }
        })

        .post('/search', bodyParser({
            extendTypes: {
              json: ['application/x-javascript'] // will parse application/x-javascript type body as a JSON string
            }}), async (ctx, next) => {
                /*  param sor serch must send in raw format JSON, this way will allow to flexible expand search and make it universal
                format request json, title for search by name films and stars for search by stars
                                {
                                    "name": [
                                        "Rock"
                                    ],
                                    "stars": [
                                        "shon connery" , "bla bla"
                                    ]
                                }
                */

            ctx.accepts('application/json', 'text');
            ctx.set('Access-Control-Allow-Origin', "*");

            let dataBody = ctx.request.rawBody;

            if (dataBody.replace(/{|}|\s/gm, '').trim() == ''){
                ctx.status = 404;
            }
            else {
                let data = await searchFilms(JSON.parse(dataBody));
                ctx.response.body = data;
                ctx.status = 200;
            }   
        });


exports.routes = router.routes();
exports.allowedMethods = router.allowedMethods();
