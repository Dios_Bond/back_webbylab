//** Constructor for pars txt file and create readable object */
module.exports = function Film(){
    this.putData = function (name, val){
         switch (name){
             case 'Title' :
                 this.title = val;
                 break;
             case 'Release Year' :
                 this.release_year = val;
                 break;
             case 'Format' :
                 this.format = val;
                 break;
             case 'Stars' :
                 this.stars = val;
                 break;
         }  
     }
 };